#!/bin/bash
# Пожалуйста прочитайте следующее:
# Скрипт разработан специально для 4PDA от Foreman (http://freize.org)
# Распространение без ведома автора запрещено!                 
# Распространение готовых бинарных фалов посредствам скрипта запрещено.
################################################################
### ЗАДАЮТСЯ В СКРИПТЕ ###
# $PROMETHEUS - Шапка
# $ST1  - наличие скриптов;
# $ST2  - наличие конфига (не реалезована, бутофория);
# $ST3  - наличие каталогов;
# $ST4  - наличие исходников;
# $ST5  - наличие toolchai (текст);
# $ST52 - toolchai для шапки;
# $FIRM - версия прошивки;
# $SKIN - наличие скина;
# $TCP  - наличие toolchai (1 или 0, служит для пропуска сборки при его присутствии);
# $DIRP - папка с скриптом Прометей;
# $DIRS - папка скриптов (относительный);
# $DIRC - папка с конфигами (относительный);
# $DIRF - папка с файлами (относительный);
# $EFTB - прощивка есть или нет;
### ПОДГРУЖЕНЫ ИЗ CONFIG ###
# $BACKUPDIR - папка бэккапов (относительный);
# $SNAPSHOT  - формат даты (относительный);
# $IPWRT     - IP адрес устройства;
# $ROOTWRT   - логин от роутера;
# $PWDR      - пароль от роутера.
# Цвета:
RED='\033[1;31m'
GREEN='\033[1;32m'
BLUE='\033[1;36m'
YELLOW='\033[1;33m'
NONE='\033[0m'
################################################################
clear
PROMETHEUS="$BLUE 
--------------------------------------------------------------------
- $NONE $GREEN 0000  0000  00000 0    0 00000 00000 0   0 00000 0   0 00000 $NONE $BLUE -
- $NONE $GREEN 0   0 0   0 0   0 00  00 0       0   0   0 0     0   0 0     $NONE $BLUE -
- $NONE $GREEN 0000  0000  0   0 0 00 0 00000   0   00000 00000 0   0 00000 $NONE $BLUE -
- $NONE $GREEN 0     0  0  0   0 0    0 0       0   0   0 0     0   0     0 $NONE $BLUE -
- $NONE $GREEN 0     0   0 00000 0    0 00000   0   0   0 00000 00000 00000 $NONE $BLUE -
-------------------------------------------------------------------- $NONE"
echo -e "$PROMETHEUS"
sleep 1
echo
# Отключаем скринсейвер
xset s off -dpms >/dev/null 2>&1
# Задаём директорию скрипта
DIRP=`pwd`
# Фикс устаревших сертификатов
cd
# Очищаем сертификаты
rm .ssh/known_hosts >/dev/null 2>&1
# Проверка дополнительных скриптов
# Проверяем наличие директории
if [ ! -d $DIRP/scripts ]; then
   mkdir $DIRP/scripts
fi
# Проверяем установлены ли скрипты
cd $DIRP/scripts
# Ищем метку
SF=`find . -type f -iname "*.sh"`
if [[ $SF == *"up1"* ]] 
then
   # Скрипты есть
   echo -e "$BLUE Скрипты:$NONE$GREEN      OK $NONE"
   ST1="$BLUE Скрипты:$NONE$GREEN      OK $NONE"
   sleep 1
   cd $DIRP
else
   # Скрипты отсутствуют
   internet_connection=$(ping -q -w 1 -c 1 google.com > /dev/null 2>&1 && echo ok || echo error)
   if [ "$internet_connection" == "error" ]
   then
      echo -e "$RED Нет соединения с интернетом, дальнейшая работа невозможна! $NONE"
      cd $DIRP
      exit
   else 
      internet_connection2=$(ping -q -w 1 -c 1 freize.net > /dev/null 2>&1 && echo ok || echo error)
      if [ "$internet_connection2" == "error" ]
      then
         echo "$RED Удаленный сервер не отвечает, дальнейшая работа невозможна! $NONE"
         cd $DIRP
         exit
      else
         echo -e "$BLUE Скрипты:$NONE$YELLOW      ОТСУТСТВУЮТ $NONE"
         ST1="$BLUE Скрипты:$NONE$YELLOW      ОТСУТСТВУЮТ $NONE"
         sleep 1
         cd $DIRP
         wget -O update.tar ftp://guest2:gu@freize.net/4GB/scripts/update-2.tar
         tar -xvf update.tar
         rm -f update.tar
         ./scripts/up2.sh
         echo -e "$BLUE Скрипты:$NONE$GREEN      OK $NONE"
         sleep 2
         exec ./start.sh
      fi
   fi
fi
# Подключаем конфиг
cd $DIRP
. ./configs/config.sh
echo -e "$BLUE Конфиг:$NONE$GREEN       OK $NONE"
ST2="$BLUE Конфиг:$NONE$GREEN       OK $NONE"
sleep 1
# Проверяем наличие xrmwrt
if [ -d $DIRP/xrmwrt* ]
then
   # Есть директория
   echo -e "$BLUE Каталоги:$NONE$GREEN     OK $NONE"
   ST3="BLUE Каталоги:$NONE$GREEN     OK $NONE"
   sleep 1
else
   # Нет директории
   echo -e "$BLUE Каталоги:$NONE$YELLOW     ОТСУТСТВУЮТ $NONE"
   ST3="$BLUE Каталоги:$NONE$YELLOW     ОТСУТСТВУЮТ $NONE"
   echo -e "$BLUE Создаем каталоги... $NONE"
   mkdir xrmwrt >/dev/null 2>&1
   echo -e "$YELLOW Устанавливаем патч, требуется ввести пароль$NONE"
   echo -e "$YELLOW от вашей учетной записи. $NONE"
   sudo rm -f /opt/xrmwrt
   sudo ln -sd $DIRP/xrmwrt/ /opt/xrmwrt
   sleep 2
   clear
   echo -e "$PROMETHEUS"
   echo -e "$ST1"
   echo -e "$ST2"
   echo -e "$BLUE Каталоги:$NONE$GREEN     OK $NONE"
   ST3="$BLUE Каталоги:$NONE$GREEN     OK $NONE"
   sleep 1
fi
# Проверяем наличие файла
if [ -f $DIRP/xrmwrt/readme.rus.txt ]
then
   echo -e "$BLUE Исходный код:$NONE$GREEN OK $NONE"
   ST4="$BLUE Исходный код:$NONE$GREEN OK $NONE"
   sleep 1
else
   # Файл не обнаружен
   internet_connection=$(ping -q -w 1 -c 1 google.com > /dev/null 2>&1 && echo ok || echo error)
   if [ "$internet_connection" == "error" ]
   then
      # нет соединения
      echo -e "$RED Нет соединения с интернетом, дальнейшая работа невозможна! $NONE"
      exit
   else 
      # Есть соединение
      echo -e "$BLUE Исходный код:$NONE$YELLOW ОТСУТСТВУЕТ $NONE"
      ST4="$BLUE Исходный код:$NONE$YELLOW ОТСУТСТВУЕТ $NONE"
      sleep 1
      echo -e "$BLUE Загружаем исходный код... $NONE"
      git clone https://gitlab.com/Track/xrmwrt.git
      echo -e "$BLUE Проверяем загрузку... $NONE"
      sleep 1
      # Наличие файла
      if [ -f $DIRP/xrmwrt/readme.rus.txt ]
      then
         # Ничего не делаем
         sleep 2
         clear
         echo -e "$PROMETHEUS"
         echo -e "$ST1"
         echo -e "$ST2"
         echo -e "$ST3"
         echo -e "$BLUE Исходный код:$NONE$GREEN OK $NONE"
         ST4="$BLUE Исходный код:$NONE$GREEN OK $NONE"
         sleep 1
      else
         # Запускаем скрипт клонирования
         ST4="$BLUE Исходный код,$NONE$RED ERROR $NONE"
         $DIRP/$DIRS/download.sh
         sleep 2
         clear
         echo -e "$PROMETHEUS"
         echo -e "$ST1"
         echo -e "$ST2"
         echo -e "$ST3"
         echo -e "$BLUE Исходный код:$NONE$GREEN OK $NONE"
         ST4="$BLUE Исходный код:$NONE$GREEN OK $NONE"
         sleep 1
      fi
   fi
fi
# Проверяем наличие toolchain
if [ -d $DIRP/xrmwrt/toolchain-mipsel/toolchain-* ]
then
   # Каталог обнаружен
   echo -e "$BLUE Toolchain:$NONE$GREEN    OK $NONE"
   ST5="$BLUE Toolchain:$NONE$GREEN    OK $NONE"
   ST52="$BLUE Toolchain:$NONE$GREEN OK$NONE"
   TCP=1
   sleep 1
else
   echo -e "$BLUE Toolchain:$NONE$YELLOW    NONE $NONE"
   ST5="$BLUE Toolchain:$NONE$YELLOW    NONE $NONE"
   ST52="$BLUE Toolchain:$NONE$YELLOW NONE$NONE"
   sleep 1
   TCP=0
fi
# Проверяем наличие прошивки
if [ -d $DIRP/xrmwrt/trunk/images* ]
then
   # Каталог обнаружен
   cd $DIRP/xrmwrt/trunk/images
   # Ищем прошивку
   FF=`find . -type f -iname "*.trx"`
   if [[ $FF == *".trx"* ]] 
   then
      FFM=`echo "$FF" | sed 's/^\.\///'`
      echo -e "$BLUE Firmware:$NONE $YELLOW    $FFM $NONE"
      FIRM="$BLUE Firmware:$NONE$YELLOW $FFM$NONE"
      sleep 1
   else
      echo -e "$BLUE Firmware:$NONE $YELLOW    NONE $NONE"
      FIRM="$BLUE Firmware:$NONE$YELLOW NONE$NONE"
      sleep 1
   fi
else 
   echo -e "$BLUE Firmware:$NONE $YELLOW    NONE $NONE"
   FIRM="$BLUE Firmware:$NONE$YELLOW NONE$NONE"
   sleep 1
fi
cd $DIRP
# Проверяем наличие скина
if [ -d $DIRP/xrmwrt/trunk/user/www* ]
then
   # Каталог обнаружен
   cd $DIRP/xrmwrt/trunk/user/www
   # Ищем метку
   SF=`find . -type f -iname "*0000*"`
   if [[ $SF == *"0000"* ]] 
   then
      echo -e "$BLUE Скин:    $NONE $YELLOW    GREY $NONE"
      SKIN="$BLUE Скин:$NONE$YELLOW GREY$NONE"
      sleep 1
   else
      echo -e "$BLUE Скин:    $NONE $GREEN    ORIGINAL $NONE"
      SKIN="$BLUE Скин:$NONE$GREEN ORIGINAL$NONE"
      sleep 1
   fi
else 
   echo -e "$BLUE Скин:    $NONE $GREEN    ORIGINAL $NONE"
   SKIN="$BLUE Скин:$NONE$GREEN ORIGINAL$NONE"
   sleep 1
fi
cd $DIRP
while :
do
    clear
echo -e "$PROMETHEUS"
sleep 1
echo -e "$BLUE $NONE"
echo -e "$BLUE Параметры подключения $NONE"
echo -e "$BLUE $NONE"
echo -e "$BLUE IP:    $NONE $YELLOW      $IPWRT $NONE"
echo -e "$BLUE Login:    $NONE $YELLOW   $ROOTWRT $NONE"
echo -e "$BLUE Password:  $NONE $YELLOW  $(echo $PWDR | sed 's/\\\"/\"/g') $NONE $BLUE"
echo -e " "
# Проверяем подключение
err=$(sshpass -p "$PWDR" ssh -T -oStrictHostKeyChecking=no $ROOTWRT@$IPWRT 'cd' 2>&1 1>/dev/null)
if [[ "$err" == *ermission* ]] 
then 
   echo -e "$RED Логин или пароль неверен! $NONE $BLUE"
   SSH=0
elif [[ "$err" == ssh* ]] 
   then 
   echo -e "$RED Возможно запрещено подключение по SSH, проверьте в настройках роутера! $NONE $BLUE"
   SSH=0
elif [[ "$err" == "usage: ssh"* ]] 
   then 
   echo -e "$RED Логин и Пароль не могут быть пустыми! $NONE $BLUE"
   SSH=0
else
   echo -e "$GREEN Удалось подключиться к роутеру... это он? $NONE $BLUE"
   SSH=1
fi
    cat<<EOF

 Данные параметры верны?
 Введите ответ:
 Данные верны    (1)
 Обновить данные (2)
                 (F)AQ
                 (Q)uit

EOF
    read -n1 -s
    case "$REPLY" in
    "1")  while :
          do
    clear

#---------------------------------------------------------------
# Шапка Начало
#--------------------------------------------------------------- 
echo -e "$BLUE 
--------------------------------------------------------------------
  $NONE$GREEN                          PROMETHEUS     $NONE$BLUE
  $NONE$RED                     Версия скрипта: 1.07r $NONE$BLUE
  Здравствуй, дорогой друг 4PDA!
  Prometheus, это пакет скриптов по обслуживанию и прошивке роутера
  MI-WIFI MINI.
  Автор скрипта: Foreman (freize), Yadoff.
  Контакты: is@freize.net
 $ST52$FIRM$SKIN
--------------------------------------------------------------------$NONE"
    cat<<EOF2
  Пожалуйста введите номер желаемого пункта:
  Обновить скрипты       (1)
  Обновить исходный код  (2)
  Собрать Toolchain      (3)
  Изменить config сборки (4)
  Применить серый скин   (5)
  Собрать Firmware       (6)
  Прошить Firmware       (7)
  Прошить патч EEPROM    (8)
  Прошить U-Boot         (9) - опасно!
                         (F)AQ
                         (Q)uit
--------------------------------------------------------------------
EOF2
    read -n1 -s
    case "$REPLY" in
    "1") echo -e "$BLUE Обновляем скрипты... $NONE"
         internet_connection2=$(ping -q -w 1 -c 1 freize.net > /dev/null 2>&1 && echo ok || echo error)
         if [ "$internet_connection2" == "error" ]
         then
             echo -e "$RED Удаленный сервер не отвечает, дальнейшая работа невозможна! $NONE"
             exit
          else 
             ./$DIRS/up1.sh
             ./$DIRS/up2.sh
             exec ./start.sh
          fi
          cd $DIRP ;;
    "2")  echo -e "$BLUE Обновляем исходный код... $NONE"
          # Проверяем наличие скина
          cd $DIRP/xrmwrt/trunk/user/www
          # Ищем метку
          SF=`find . -type f -iname "*0000*"`
          if [[ $SF == *"0000"* ]] 
          then
             echo -e "$BLUE Возвращаем класический скин... $NONE"
             cd $DIRP
             rm -rf $DIRP/xrmwrt/trunk/user/www >/dev/null 2>&1
             tar -xf $DIRP/$DIRF/www.tar.gz >/dev/null 2>&1
             SKIN="$BLUE Скин:$NONE$GREEN ORIGINAL$NONE"
             sleep 1
          else
             sleep 1
          fi
          cd $DIRP/xrmwrt
          git checkout .
          git pull
          sleep 2
          cd $DIRP ;;
    "3")  echo -e "$BLUE Собираем toolchain... $NONE"
          cd $DIRP/xrmwrt/toolchain-mipsel
          #if [ $TCP -gt 0 ]
          #then
          #   ./clean_toolchain
          #else
          #   echo -e "$BLUE Toolchain,$NONE$YELLOW NONE $NONE"
          #fi
          ./build_toolchain
          # Проверяем наличие toolchain
          if [ -d $DIRP/xrmwrt/toolchain-mipsel/toolchain-* ]
          then
             # Каталог обнаружен
             echo -e "$BLUE Toolchain:$NONE$GREEN OK$NONE"
             ST52="$BLUE Toolchain:$NONE$GREEN OK$NONE"
             TCP=1
             sleep 1
          else
             echo -e "$BLUE Toolchain:$NONE$RED ERROR$NONE"
             ST52="$BLUE Toolchain:$NONE$RED ERROR$NONE"
             echo -e "$BLUE В процессе сборки Toolchain произошла ошибка! $NONE"
             TCP=0
             sleep 4
          fi
          cd $DIRP ;;
    "4")  echo -e "$BLUE Настриваем Config сборки... $NONE"
          echo -e "$RED Измените открывшийся файл, решетка перед $NONE"
          echo -e "$RED параметром его выключает... $NONE"
          sleep 7
          nano $DIRP/$DIRC/config-b.sh
          cp -f $DIRP/$DIRC/config-b.sh $DIRP/xrmwrt/trunk/.config
          echo -e "$BLUE Config,$NONE$GREEN OK $NONE"
          # Эксперементальная функция
          echo -e "$BLUE Хотите задействовать красный диод для WAN и кнопку RESET?$NONE$RED"
          # Тут диалог да/нет или автоматизация
          while true; do
              read -p " Изменить настройку? (экспериментально) " yn
              case $yn in
                  [Yy]* ) REDW=1 ; break;;
                  [Nn]* ) REDW=0 ; break;;
                  * ) echo -e " Пожалуйста введите yes или no.";;
              esac
          done
          if [ $REDW -gt 0 ]
          then
             # Меняем файл
             echo -e "$RED Открывшийся файл можно не менять.\n" \
             "define - активно, undef - пассивно.$NONE"
             sleep 5
             nano $DIRP/$DIRC/board.h
             cp -fp $DIRP/$DIRC/board.h $DIRP/xrmwrt/trunk/configs/boards/MI-MINI/
             echo -e "$BLUE Патч,$NONE$GREEN OK $NONE"
          else
             # Отмена
             echo -e "$BLUE Оставляем без изменения. $NONE"
          fi
          cd $DIRP ;;
    "5")  # Проверяем наличие скина
          cd $DIRP/xrmwrt/trunk/user/www
          # Ищем метку
          SF=`find . -type f -iname "*0000*"`
          cd $DIRP
          if [[ $SF == *"0000"* ]] 
          then
             echo -e "$BLUE Возвращаем класический скин... $NONE"
             rm -rf $DIRP/xrmwrt/trunk/user/www >/dev/null 2>&1
             tar -xf $DIRP/$DIRF/www.tar.gz >/dev/null 2>&1
             echo -e "$BLUE Скин:$NONE$GREEN ORIGINAL$NONE"
             SKIN="$BLUE Скин:$NONE$GREEN ORIGINAL$NONE"
             sleep 1
          else
             echo -e "$BLUE Подготавливаем серый скин... $NONE"
             echo -e "$BLUE Создаём бэккап... $NONE"
             tar -czf $DIRP/$DIRF/www.tar.gz xrmwrt/trunk/user/www
             echo -e "$BLUE Очищаем предыдущий... $NONE"
             rm -rf $DIRP/xrmwrt/trunk/user/www >/dev/null 2>&1
             echo -e "$BLUE Устанавливаем... $NONE"
             unzip $DIRP/$DIRF/grey.zip -d $DIRP/xrmwrt/trunk/user/ >/dev/null 2>&1
             echo -e "$BLUE Скин:$NONE$YELLOW GREY$NONE"
             SKIN="$BLUE Скин:$NONE$YELLOW GREY$NONE"
             sleep 1
          fi
          cd $DIRP ;;
    "6")  cd $DIRP/xrmwrt/toolchain-mipsel
          if [ $TCP -gt 0 ]
          then
             echo -e "$BLUE Toolchain,$NONE$GREEN OK $NONE"
          else
             ./build_toolchain
             # Проверяем наличие toolchain
             if [ -d $DIRP/xrmwrt/toolchain-mipsel/toolchain-* ]
             then
                # Каталог обнаружен
                echo -e "$BLUE Toolchain:$NONE$GREEN OK$NONE"
                ST52="$BLUE Toolchain:$NONE$GREEN OK$NONE"
                TCP=1
                sleep 1
             else
                echo -e "$BLUE Toolchain:$NONE$RED ERROR$NONE"
                ST52="$BLUE Toolchain:$NONE$RED ERROR$NONE"
                echo -e "$RED В процессе сборки Toolchain произошла ошибка! $NONE"
                TCP=0
                echo -e "$RED Скрипт останавливается. $NONE"
                exit
             fi
          fi
          # Проверяем наличие прошивки
          if [ -d $DIRP/xrmwrt/trunk/images* ]
          then
             # Каталог обнаружен
             cd $DIRP/xrmwrt/trunk/images
             # Ищем прошивку
             FF=`find . -type f -iname "*.trx"`
             if [[ $FF == *".trx"* ]] 
             then
                # Прошивка найдена
                EFTB=1
             else
                # Прошивки нет
                EFTB=0
             fi
          else
             # Прошивки нет
             EFTB=0
          fi
          if [ $EFTB -gt 0 ]
          then
             # Эксперементальная функция
             echo -e "$BLUE Вы уже собирали прошивку... $NONE"
             echo -e "$BLUE Быстрая сборка доступна только для этой же версии кода.$NONE $RED"
             # Тут диалог да/нет или автоматизация
             while true; do
                 read -p " Попробовать ускореную сборку? (экспериментально) " yn
                 case $yn in
                     [Yy]* ) EFTB2=1 ; break;;
                     [Nn]* ) EFTB2=0 ; break;;
                     * ) echo -e " Пожалуйста введите yes или no.";;
                 esac
             done
             echo -e " $NONE"
             if [ $EFTB2 -gt 0 ]
             then
                FFM=`echo "$FF" | sed 's/^\.\///'`
                rm -f $DIRP/xrmwrt/trunk/images/$FFM
                cd $DIRP/xrmwrt/trunk
                make -C user/httpd clean
                make -C user/rc clean
                make -C user/shared clean
                make
             else
                # Меняем ответ
                EFTB=0
             fi
          fi
          if [ $EFTB -gt 0 ]
          then
             echo
          else
             # Собираем полностью
             cd $DIRP/xrmwrt/trunk
             echo -e "$BLUE Чистим... $NONE"
             ./clear_tree
             echo -e "$BLUE Собираем firmware... $NONE"
             ./build_firmware
          fi
          # Проверяем готовность прошивки
          if [ -d $DIRP/xrmwrt/trunk/images* ]
          then
             # Каталог обнаружен
             cd $DIRP/xrmwrt/trunk/images
             # Ищем прошивку
             FF=`find . -type f -iname "*.trx"`
             if [[ $FF == *".trx"* ]] 
             then
                FFM=`echo "$FF" | sed 's/^\.\///'`
                echo -e "$BLUE Firmware:$NONE$YELLOW $FFM $NONE"
                FIRM="$BLUE Firmware:$NONE$YELLOW $FFM$NONE"
               sleep 1
             else
                echo -e "$BLUE Firmware:$NONE$RED ERROR $NONE"
                echo -e "$BLUE Похоже, при компиляции прошивки произошла ошибка, попробуйте перекачать исходники, спасибо. $NONE"
                FIRM="$BLUE Firmware:$NONE$RED ERROR$NONE"
                sleep 10
             fi
          else
             echo -e "$BLUE Firmware:$NONE$RED ERROR$NONE"
             echo -e "$BLUE Похоже, при компиляции прошивки произошла ошибка, попробуйте перекачать исходники, спасибо. $NONE"
             FIRM="$BLUE Firmware:$NONE$RED ERROR$NONE"
             sleep 10
          fi
          cd $DIRP ;;
    "7")  if [ $SSH -gt 0 ]
          then
             echo -e "$BLUE Доступ по SSH предоставлен... $NONE"
          else
             echo -e "$RED Ошибка доступа SSH, перезапустите скрипт для повторной настройки... $NONE"
             sleep 5
             exec ./start.sh
          fi
          echo -e "$BLUE Подготавливаем бэккап... $NONE"
          if [ ! -d $DIRP/$BACKUPDIR ]; then
             mkdir $DIRP/$BACKUPDIR
          fi
          if [ -d $DIRP/$BACKUPDIR/$SNAPSHOT* ]
          then
             # Каталог обнаружен
             echo -e "$BLUE Бэккап уже есть от $SNAPSHOT $NONE"
          else
             if [ ! -d $DIRP/$BACKUPDIR/$SNAPSHOT ]; then
                mkdir $DIRP/$BACKUPDIR/$SNAPSHOT
             fi
             all_mtds=$(sshpass -p "$PWDR" ssh -T -oStrictHostKeyChecking=no $ROOTWRT@$IPWRT 'cat /proc/mtd' | egrep "^mtd([0-9])+" -o)
             while read -r mtd;
             do
             echo -e "$BLUE Дампим /dev/$mtd в $SNAPSHOT  $NONE"
             sshpass -p "$PWDR" ssh -T -oStrictHostKeyChecking=no $ROOTWRT@$IPWRT "cat /dev/$mtd" < /dev/null > ./$BACKUPDIR/$SNAPSHOT/$mtd.bin
             backup_error=0
             if [ -s ./$BACKUPDIR/$SNAPSHOT/$mtd.bin ]
             then
                remote_md5=$(sshpass -p "$PWDR" ssh -T -oStrictHostKeyChecking=no $ROOTWRT@$IPWRT "md5sum /dev/$mtd" < /dev/null | sed 's/ .*//')
                local_md5=$(md5sum ./$BACKUPDIR/$SNAPSHOT/$mtd.bin | sed 's/ .*//') 
                if [ "$remote_md5" != "$local_md5" ]
                then
                   echo -e "$BLUE Checksum,$NONE$RED ERROR $NONE"
                   rm ./$BACKUPDIR/$SNAPSHOT/$mtd.bin
                   backup_error=1
                else
                   echo -e "$BLUE Checksum,$NONE$GREEN OK $NONE"
                fi
             else
                echo -e "$RED Файл не существует или пуст!!! $NONE"
                rm ./$BACKUPDIR/$SNAPSHOT/$mtd.bin
                backup_error=1
              fi
              done <<< "$all_mtds"
              if [ $backup_error == 1 ]
             then
                echo -e "$RED Что-то пошло не так, не удалось создать бэккап! $NONE"
             else 
                echo -e "$GREEN Бэккап сохранён в ./$BACKUPDIR/$SNAPSHOT $NONE"
             fi
          fi
          firmware_size=$(stat -c %s $DIRP/xrmwrt/trunk/images/$FFM)
          local_md5=$(md5sum $DIRP/xrmwrt/trunk/images/$FFM | sed 's/ .*//')
          remote_md5=$(dd if=./$BACKUPDIR/$SNAPSHOT/mtd6.bin bs=1 count=$firmware_size 2>/dev/null | md5sum | sed 's/ .*//')
          if [ "$local_md5" == "$remote_md5" ]
          then
             echo -e "$RED Роутер уже прошит именно этим билдом $FFM... $NONE $BLUE"
             while true; do
                 read -p "Все равно прошить заново? " yn
                 case $yn in
                     [Yy]* ) force_flashing=1 ; break;;
                     [Nn]* ) force_flashing=0 ; break;;
                     * ) echo -e "Пожалуйста введите yes или no.";;
                 esac
             done
             echo -e " $NONE"
          else
             force_flashing=1
          fi
          if [ $force_flashing == 1 ]
          then
             echo -e "$BLUE Загружаем прошивку... $NONE"
             sshpass -p "$PWDR" scp -oStrictHostKeyChecking=no $DIRP/xrmwrt/trunk/images/$FFM $ROOTWRT@$IPWRT:/tmp/
             # Проверяем md5
             echo -e "$BLUE Проверяем Checksum... $NONE"
             local_md5=$(md5sum $DIRP/xrmwrt/trunk/images/$FFM | sed 's/ .*//')
             remote_md5=$(sshpass -p "$PWDR" ssh -T -oStrictHostKeyChecking=no $ROOTWRT@$IPWRT "md5sum /tmp/$FFM" < /dev/null | sed 's/ .*//')
             if [ "$remote_md5" != "$local_md5" ]
             then
                echo -e "$BLUE Checksum,$NONE$RED ERROR $NONE"
                echo -e "$RED Для предотвращения повреждения роутера, скрипт останавливается! $NONE"
                exit
             else
                echo -e "$BLUE Checksum,$NONE$GREEN OK $NONE"
             fi
             # Определяем раздел куда шить
             echo -e "$BLUE Определяем раздел... $NONE"
             proc_mtd=$(sshpass -p "$PWDR" ssh -T -oStrictHostKeyChecking=no $ROOTWRT@$IPWRT 'cat /proc/mtd')
             if [[ $proc_mtd == *"Firmware_Stub"* ]] 
             then 
                export_script="cd /tmp; mtd_write write $FF Firmware_Stub;"
             elif [[ $proc_mtd == *"firmware"* ]] 
             then 
                export_script="cd /tmp; mtd write $FF firmware;"
             else 
                export_script="cd /tmp; mtd write $FF OS1;"
             fi
             # Прошиваем
             echo -e "$BLUE Прошиваем... $NONE"
             echo -e "$BLUE Роутер будет доступен (после перезагрузки) по адресу 192.168.1.1,\n" \
                     "логин - admin, пароль - admin; WiFi сеть - ASUS,\n" \
                     "пароль по умолчанию - 1234567890.\n" \
                     "Не забудьте включить доступ по SSH для обновлений прошивки. $NONE"
             sshpass -p "$PWDR" ssh -T -oStrictHostKeyChecking=no $ROOTWRT@$IPWRT $export_script
             sleep 3
          else 
             echo -e "$BLUE Действие отменено. $NONE"
             sleep 3
          fi
          cd $DIRP ;;
    "8")  if [ $SSH -gt 0 ]
          then
             echo -e "$BLUE Доступ по SSH предоставлен... $NONE"
          else
             echo -e "$RED Ошибка доступа SSH, перезапустите скрипт для повторной настройки... $NONE"
             sleep 5
             exec ./start.sh
          fi
          echo -e "$BLUE Подготавливаем бэккап... $NONE"
          if [ ! -d $DIRP/$BACKUPDIR ]; then
             mkdir $DIRP/$BACKUPDIR
          fi
          if [ -d $DIRP/$BACKUPDIR/$SNAPSHOT* ]
          then
             # Каталог обнаружен
             echo -e "$BLUE Бэккап уже есть от $SNAPSHOT $NONE"
          else
             if [ ! -d $DIRP/$BACKUPDIR/$SNAPSHOT ]; then
                mkdir $DIRP/$BACKUPDIR/$SNAPSHOT
             fi
             all_mtds=$(sshpass -p "$PWDR" ssh -T -oStrictHostKeyChecking=no $ROOTWRT@$IPWRT 'cat /proc/mtd' | egrep "^mtd([0-9])+" -o)
             while read -r mtd;
             do
             echo -e "$BLUE Дампим /dev/$mtd в $SNAPSHOT  $NONE"
             sshpass -p "$PWDR" ssh -T -oStrictHostKeyChecking=no $ROOTWRT@$IPWRT "cat /dev/$mtd" < /dev/null > ./$BACKUPDIR/$SNAPSHOT/$mtd.bin
             backup_error=0
             if [ -s ./$BACKUPDIR/$SNAPSHOT/$mtd.bin ]
             then
                remote_md5=$(sshpass -p "$PWDR" ssh -T -oStrictHostKeyChecking=no $ROOTWRT@$IPWRT "md5sum /dev/$mtd" < /dev/null | sed 's/ .*//')
                local_md5=$(md5sum ./$BACKUPDIR/$SNAPSHOT/$mtd.bin | sed 's/ .*//') 
                if [ "$remote_md5" != "$local_md5" ]
                then
                   echo -e "$BLUE Checksum,$NONE$RED ERROR $NONE"
                   rm ./$BACKUPDIR/$SNAPSHOT/$mtd.bin
                   backup_error=1
                else
                   echo -e "$BLUE Checksum,$NONE$GREEN OK $NONE"
                fi
             else
                echo -e "$RED Файл не существует или пуст!!! $NONE"
                rm ./$BACKUPDIR/$SNAPSHOT/$mtd.bin
                backup_error=1
              fi
              done <<< "$all_mtds"
              if [ $backup_error == 1 ]
             then
                echo -e "$RED Что-то пошло не так, не удалось создать бэккап! $NONE"
             else 
                echo -e "$GREEN Бэккап сохранён в ./$BACKUPDIR/$SNAPSHOT $NONE"
             fi
          fi
          # Получаем раздел где лежит Factory
          echo -e "$BLUE Проверяем EEPROM... $NONE"
          factory_mtd=$(sshpass -p "$PWDR" ssh -T -oStrictHostKeyChecking=no $ROOTWRT@$IPWRT 'cat /proc/mtd' | grep Factory | egrep "^mtd([0-9])+" -o)
          byte8045=$(xxd -s 0x8045 -l 1 -ps ./$BACKUPDIR/$SNAPSHOT/$factory_mtd.bin)
          byte8049=$(xxd -s 0x8049 -l 1 -ps ./$BACKUPDIR/$SNAPSHOT/$factory_mtd.bin)
          byte804D=$(xxd -s 0x804D -l 1 -ps ./$BACKUPDIR/$SNAPSHOT/$factory_mtd.bin)
          if [ "$byte8045" != "00" ] || [ "$byte8049" != "00" ] || [ "$byte804D" != "00" ]
          then 
             echo -e "$RED Обнаружен заводской EEPROM, рекомендуется накатить патч!  $NONE"
             # Тут диалог или автоматизация если тихая установка
             echo -e "$BLUE Сделать это сейчас? "
             while true; do
                 read -p " Вы желаете прошить EEPROM? " yn
                 case $yn in
                     [Yy]* ) EE1=1 ; break;;
                     [Nn]* ) EE1=0 ; break;;
                     * ) echo -e " Пожалуйста введите yes или no.";;
                 esac
             done
             echo -e " $NONE"
             if [ $EE1 -gt 0 ]
             then
                cp -f ./$BACKUPDIR/$SNAPSHOT/$factory_mtd.bin factory_mtd.bin
                printf '\x00' | dd conv=notrunc of=factory_mtd.bin bs=1 seek=$((0x8045))
                printf '\x00' | dd conv=notrunc of=factory_mtd.bin bs=1 seek=$((0x8049))
                printf '\x00' | dd conv=notrunc of=factory_mtd.bin bs=1 seek=$((0x804D))
                # Загружаем EEPROM в роутер
                echo -e "$BLUE Загружаем EEPROM... $NONE"
                sshpass -p "$PWDR" scp -oStrictHostKeyChecking=no factory_mtd.bin $ROOTWRT@$IPWRT:/tmp/
                # Проверяем md5
                echo -e "$BLUE Проверяем Checksum... $NONE"
                remote_md5=$(sshpass -p "$PWDR" ssh -T -oStrictHostKeyChecking=no $ROOTWRT@$IPWRT "md5sum /tmp/factory_mtd.bin" < /dev/null | sed 's/ .*//')
                local_md5=$(md5sum factory_mtd.bin | sed 's/ .*//') 
                if [ "$remote_md5" != "$local_md5" ]
                then
                   echo -e "$BLUE Checksum,$NONE$RED ERROR $NONE"
                   rm factory_mtd.bin
                   exit
                else
                   echo -e "$BLUE Checksum,$NONE$GREEN OK $NONE"
                   # Прошиваем
                   echo -e "$BLUE Прошиваем... $NONE"
                   export_script="mtd write /tmp/factory_mtd.bin Factory || mtd_write write /tmp/factory_mtd.bin Factory"
                   sshpass -p "$PWDR" ssh -T -oStrictHostKeyChecking=no $ROOTWRT@$IPWRT $export_script
                   echo -e "$GREEN OK $NONE"
                   rm factory_mtd.bin
                fi
             else
                echo -e "$BLUE Оставляем как есть... $NONE"
             fi
          else
             echo -e "$BLUE EEPROM,$NONE$GREEN ОК $NONE"
          fi
          cd $DIRP
          ;;
    "9")  if [ $SSH -gt 0 ]
          then
             echo -e "$BLUE Доступ по SSH предоставлен... $NONE"
          else
             echo -e "$RED Ошибка доступа SSH, перезапустите скрипт для повторной настройки... $NONE"
             sleep 5
             exec ./start.sh
          fi
          echo -e "$BLUE Подготавливаем бэккап... $NONE"
          if [ ! -d $DIRP/$BACKUPDIR ]; then
             mkdir $DIRP/$BACKUPDIR
          fi
          if [ -d $DIRP/$BACKUPDIR/$SNAPSHOT* ]
          then
             # Каталог обнаружен
             echo -e "$BLUE Бэккап уже есть от $SNAPSHOT $NONE"
          else
             if [ ! -d $DIRP/$BACKUPDIR/$SNAPSHOT ]; then
                mkdir $DIRP/$BACKUPDIR/$SNAPSHOT
             fi
             all_mtds=$(sshpass -p "$PWDR" ssh -T -oStrictHostKeyChecking=no $ROOTWRT@$IPWRT 'cat /proc/mtd' | egrep "^mtd([0-9])+" -o)
             while read -r mtd;
             do
             echo -e "$BLUE Дампим /dev/$mtd в $SNAPSHOT  $NONE"
             sshpass -p "$PWDR" ssh -T -oStrictHostKeyChecking=no $ROOTWRT@$IPWRT "cat /dev/$mtd" < /dev/null > ./$BACKUPDIR/$SNAPSHOT/$mtd.bin
             backup_error=0
             if [ -s ./$BACKUPDIR/$SNAPSHOT/$mtd.bin ]
             then
                remote_md5=$(sshpass -p "$PWDR" ssh -T -oStrictHostKeyChecking=no $ROOTWRT@$IPWRT "md5sum /dev/$mtd" < /dev/null | sed 's/ .*//')
                local_md5=$(md5sum ./$BACKUPDIR/$SNAPSHOT/$mtd.bin | sed 's/ .*//') 
                if [ "$remote_md5" != "$local_md5" ]
                then
                   echo -e "$BLUE Checksum,$NONE$RED ERROR $NONE"
                   rm ./$BACKUPDIR/$SNAPSHOT/$mtd.bin
                   backup_error=1
                else
                   echo -e "$BLUE Checksum,$NONE$GREEN OK $NONE"
                fi
             else
                echo -e "$RED Файл не существует или пуст!!! $NONE"
                rm ./$BACKUPDIR/$SNAPSHOT/$mtd.bin
                backup_error=1
              fi
              done <<< "$all_mtds"
              if [ $backup_error == 1 ]
             then
                echo -e "$RED Что-то пошло не так, не удалось создать бэккап! $NONE"
             else 
                echo -e "$GREEN Бэккап сохранён в ./$BACKUPDIR/$SNAPSHOT $NONE"
             fi
          fi
          echo -e "$BLUE Подготавливаем U-Boot... $NONE"
          uboot_old_version=$(cat ./$BACKUPDIR/$SNAPSHOT/mtd0.bin | tr "\0" "\n" | egrep -o "^(U-Boot ([0-9]{1}\.[0-9]{1}\.[0-9]{1}){1} .*|([0-9]{1}\.[0-9]{1}\.[0-9]{1}\.[0-9]{1}){1})$")
          echo -e "$BLUE Текущий загрузчик в роутере: $uboot_old_version $NONE"
          cd $DIRP/xrmwrt/uboot/mips/profiles/xiaomi_mi-mini
          if [ -s uboot.bin ] && [ -s uboot.md5 ]
          then
             uboot_new_version=$(cat uboot.bin | tr "\0" "\n" | egrep -o "^(U-Boot ([0-9]{1}\.[0-9]{1}\.[0-9]{1}){1} .*|([0-9]{1}\.[0-9]{1}\.[0-9]{1}\.[0-9]{1}){1})$")
             if [ "$uboot_new_version" != "$uboot_old_version" ]
             then
                echo -e "$RED Обнаруженн новый загрузчик: $uboot_new_version $NONE $BLUE"
                # Тут диалог да/нет или автоматизация
                while true; do
                    read -p " Вы желаете прошить U-Boot? " yn
                    case $yn in
                        [Yy]* ) UBT=1 ; break;;
                        [Nn]* ) UBT=0 ; break;;
                        * ) echo -e " Пожалуйста введите yes или no.";;
                    esac
                done
                echo -e " $NONE"
                if [ $UBT -gt 0 ]
                then
                   # Загружаем загрузчик в роутер
                   echo -e "$BLUE Загружаем Bootloader... $NONE"
                   sshpass -p "$PWDR" scp -oStrictHostKeyChecking=no uboot.bin $ROOTWRT@$IPWRT:/tmp/
                   # Проверяем md5
                   echo -e "$BLUE Проверяем Checksum... $NONE"
                   remote_md5=$(sshpass -p "$PWDR" ssh -T -oStrictHostKeyChecking=no $ROOTWRT@$IPWRT "md5sum /tmp/uboot.bin" < /dev/null | sed 's/ .*//')
                   local_md5=$(cat uboot.md5 | sed 's/ .*//') 
                   if [ "$remote_md5" != "$local_md5" ]
                   then
                      echo -e "$BLUE Checksum,$NONE$RED ERROR $NONE"
                   else
                      echo -e "$BLUE Checksum,$NONE$GREEN OK $NONE"
                      # Прошиваем
                      echo -e "$BLUE Прошиваем... $NONE"
                      export_script="mtd write /tmp/uboot.bin Bootloader || mtd_write write /tmp/uboot.bin Bootloader"
                      sshpass -p "$PWDR" ssh -T -oStrictHostKeyChecking=no $ROOTWRT@$IPWRT $export_script
                      echo -e "$GREEN OK $NONE"
                   fi
                else
                   echo -e "$BLUE Отмена обновления. $NONE"
                fi
             else
                echo -e "$GREEN Ваш U-Boot актуален! $NONE"
             fi
          else 
             echo -e "$RED U-Boot не найден! $NONE"
          fi
          cd $DIRP ;;
    "F")  clear
          echo -e "$YELLOW Пожалуйста, выбирите нужный пункт. Если не знаете какой, нажимайте\n" \
          "все подряд, с верху и донизу. Только перекреститься не забудьте...\n" \
          "\n" \
          "Обновить скрипты       (1) - обновит скрипт, но сбросит настройки.\n" \
          "Обновить исходный код  (2) - нужен для обнов., делать перед 3 и 4.\n" \
          "Собрать Toolchain      (3) - нужен для обнов., раз в месяц не чаще.\n" \
          "Изменить config сборки (4) - патчит конфиг сборки, желательно.\n" \
          "Применить серый скин   (5) - красивый скин, рекомендуется.\n" \
          "Собрать Firmware       (6) - сборка прошивки (Toolchain).\n" \
          "Прошить Firmware       (7) - заливает прошивку в роутер.\n" \
          "Прошить патч EEPROM    (8) - делается один раз в жизни.\n" \
          "Прошить U-Boot         (9) - опасно, не делать...\n" \
          "                             не делать, опасно... =) $NONE"
          sleep 4 ;;
    "f")  echo -e "$YELLOW С большой буквы, пожалуйста! $NONE" ;;
    "Q")  echo -e "$YELLOW Перезагружаем роутер и завершаем скрипт! $NONE"
          if [ $SSH -gt 0 ]
          then
             echo -e "$YELLOW Доступ по SSH предоставлен... $NONE"
             sshpass -p "$PWDR" ssh -T -oStrictHostKeyChecking=no $ROOTWRT@$IPWRT /sbin/reboot
             echo -e "$YELLOW DONE! $NONE"
             exit
          else
             echo -e "$YELLOW Доступ по SSH не получен, просто завершаем скрипт... $NONE"
             exit
          fi
          ;;
    "q")  echo -e "$YELLOW С большой буквы, пожалуйста! $NONE";; 
     * )  echo -e "$RED Нет такой команды! $NONE";;
    esac
    sleep 4
done
#---------------------------------------------------------------
# Шапка Конец
#---------------------------------------------------------------
          ;;
    "2")  clear
          echo -e "$PROMETHEUS"
          echo -e "$BLUE  Введите данные $NONE"
          # Параметры роутера
          # Параметры роутера
          # IP роутера(192.168.1.1 для xrmwrt или 192.168.31.1 для стока)
          echo -e "$BLUE  Введите IP роутера: $NONE"
          echo -e "$BLUE  Пример 192.168.1.1 или 192.168.31.1 $NONE"
          read IPWRT
          echo -e "$GREEN  ОК $NONE"
          # Логин роутера (admin для xrmwrt или root для стока)
          echo -e "$BLUE  Введите логин от роутера: $NONE"
          read ROOTWRT
          echo -e "$GREEN  ОК $NONE"
          # Пароль роутера (admin для xrmwrt или то, что вы получили на сеайте для стока)
          echo -e "$BLUE  Введите пароль от роутера: $NONE"
          read PWDR
          echo -e "$GREEN  ОК $NONE"
          # Задаём параметры замены
          config=config.sh
          cd $DIRC
          fname=$(mktemp);cat $config | sed s/IPWRT=.*/IPWRT=$IPWRT/ > $fname;cp $fname $config;rm -f $fname;
          fname=$(mktemp);cat $config | sed s/ROOTWRT=.*/ROOTWRT=$ROOTWRT/ > $fname;cp $fname $config;rm -f $fname;
          fname=$(mktemp);cat $config | sed s/PWDR=.*/PWDR=\"$(echo $PWDR | sed 's/\"/\\\\"/g')\"/ > $fname;cp $fname $config;rm -f $fname;
          cd $DIRP ;;
    "F")  clear
          echo -e "$YELLOW Для заводской прошивки введите данные с сайта xiaomi, а для xrmwrt\n" \
                  "укажите логин и пароль от веб интерфейса, предварительно включив\n" \
                  "доступ по SSH в админке. $NONE"
          sleep 3 ;;
    "f")  echo -e "$YELLOW С большой буквы, пожалуйста! $NONE";;
    "Q")  echo -e "$NONE"
          exit ;;
    "q")  echo -e "$YELLOW С большой буквы, пожалуйста! $NONE";; 
     * )  echo -e "$RED Нет такой команды! $NONE";;
    esac
    sleep 3
done
#---------------------------------------------------------------
# Конец скрипта
#---------------------------------------------------------------


